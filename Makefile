default: clean build-server run-server

update-website: clean update copy-website update-website-mod-dump build-client update-website-downloads

copy-website:
	cp -r website/* /var/www/html/nigeriaclub

update:
	git pull
	- rm client/mods/*.jar
	- rm server/mods/*.jar
	git checkout -- client/mods/simplelogin-1.2.4-beta.jar server/mods/DynmapBlockScan-3.0-SNAPSHOT-forge-1.12.2.jar client/mods/OptiFine_1.12.2_HD_U_G5.jar
	make install-mods

update-website-mod-dump:
	cd moddump; npm i
	cd moddump; node dumpMods.js /var/www/html/nigeriaclub/index.html

update-website-downloads:
	cp -r client-exports/* /var/www/html/nigeriaclub/downloads

install-mods:
	cd client/mods; ../../bin/modsman-cli-0.32.1/bin/modsman-cli reinstall-all
	cd server/mods; ../../bin/modsman-cli-0.32.1/bin/modsman-cli reinstall-all

install-dev:
	- rm -rf bin
	mkdir bin
	cd bin; wget https://github.com/sargunv/modsman/releases/download/0.32.1/modsman-cli-0.32.1.zip
	cd bin; unzip modsman-cli-0.32.1.zip

download-server:
	- rm -rf serverFiles
	mkdir serverFiles
	cd serverFiles; wget https://maven.minecraftforge.net/net/minecraftforge/forge/1.12.2-14.23.5.2855/forge-1.12.2-14.23.5.2855-installer.jar
	cd serverFiles; java -jar forge*installer.jar --installServer
	cd serverFiles; rm forge*installer.jar forge*installer.jar.log

clean:
	- rm -rf server-build
	- rm -rf client-build
	- rm -rf client-exports

build-server:
	mkdir server-build
	cp -r server/* server-build # serverside mods/configs
	cp -r serverFiles/* server-build # server
	cp -r client/* server-build # most files
	cd server-build; xargs -a ../clientside.txt rm -rf # remove clientside-only files
	- cp -r ../untrackedServerFiles/* server-build

run-server:
	cd server-build; java -Xms1G -Xmx4G -Dfml.readTimeout=120 -jar forge-1.12.2*.jar nogui

test-server: clean build-server
	cd server-build; echo "stop" | java -Xms1G -Xmx4G -jar forge-1.12.2*.jar nogui
	grep '\[Server thread/INFO\] \[net.minecraft.server.dedicated.DedicatedServer\]: Done' server-build/logs/latest.log

build-client:
	cp -r client client-build
	mkdir client-exports
	mkdir client-build/bin
	wget -O client-build/bin/modpack.jar https://maven.minecraftforge.net/net/minecraftforge/forge/1.12.2-14.23.5.2855/forge-1.12.2-14.23.5.2855-installer.jar
	cd client-build; zip -r ../client-exports/techniclauncher.zip *
	rm -rf client-build/bin
	mv client-build nigeriacraft
	cd nigeriacraft; zip -r ../client-exports/nigeriacraft.zip *
	mv nigeriacraft client-build

