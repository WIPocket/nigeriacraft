// aa storage crates
// "small" crate
recipes.removeByRecipeName("actuallyadditions:recipes162");
mods.thermalexpansion.InductionSmelter.addRecipe(<actuallyadditions:block_giant_chest>, <ironchest:iron_chest:2>, <actuallyadditions:item_crystal_empowered:2> * 4, 6000);
// medium crate
recipes.removeByRecipeName("actuallyadditions:giant_chest_media");
mods.thermalexpansion.InductionSmelter.addRecipe(<actuallyadditions:block_giant_chest_medium>, <actuallyadditions:block_giant_chest>, <actuallyadditions:item_crystal_empowered:2> * 10, 6000);
// large crate
recipes.removeByRecipeName("actuallyadditions:giant_chest_large");
mods.thermalexpansion.InductionSmelter.addRecipe(<actuallyadditions:block_giant_chest_large>, <actuallyadditions:block_giant_chest_medium>, <actuallyadditions:block_crystal_empowered:2> * 2, 6000);

// roost
recipes.removeByRecipeName("roost:roost");
recipes.addShaped("roost_roost", <roost:roost>, [[<extrautils2:decorativesolidwood:1>, <extrautils2:decorativesolidwood:1>, <extrautils2:decorativesolidwood:1>], [<extrautils2:decorativesolidwood:1>, <actuallyadditions:item_misc:19>, <extrautils2:decorativesolidwood:1>], [<minecraft:hay_block>, <minecraft:hay_block>, <minecraft:hay_block>]]);

// chicken breeder
recipes.removeByRecipeName("roost:breeder");
mods.thermalexpansion.InductionSmelter.addRecipe(<roost:breeder>, <roost:roost>, <twilightforest:magic_beans>, 8000);

// roost collector
recipes.removeByRecipeName("roost:collector");
recipes.addShaped("roost_collector", <roost:collector>, [[<extrautils2:decorativesolidwood:1>, <roost:chicken>, <extrautils2:decorativesolidwood:1>], [<extrautils2:decorativesolidwood:1>, <natura:blaze_hopper>, <extrautils2:decorativesolidwood:1>], [<extrautils2:decorativesolidwood:1>, <ironchest:iron_chest:5>, <extrautils2:decorativesolidwood:1>]]);

// chicken catcher
recipes.removeByRecipeName("roost:catcher");
recipes.addShaped("roost_catcher", <roost:catcher>, [[<minecraft:dragon_egg>], [<mekanism:polyethene:3>], [<twilightforest:raven_feather>]]);
<roost:catcher>.maxDamage = 16;

// dragon eggy
<minecraft:dragon_egg>.displayName = "Dragon Eggy";
recipes.addShapeless("dragon_eggy", <minecraft:dragon_egg>, [<minecraft:egg>, <minecraft:dragon_breath>, <quark:enderdragon_scale>]);

// angel rings
recipes.removeByRecipeName("extrautils2:angel_ring_0");
recipes.removeByRecipeName("extrautils2:angel_ring_1");
recipes.removeByRecipeName("extrautils2:angel_ring_2");
recipes.removeByRecipeName("extrautils2:angel_ring_3");
recipes.removeByRecipeName("extrautils2:angel_ring_4");
recipes.removeByRecipeName("extrautils2:angel_ring_5");

recipes.addShaped("angel_ring_0", <extrautils2:angelring>, [[<ore:blockGlass>, <ore:ingotGold>, <ore:blockGlass>], [<ore:ingotGold>, <extrautils2:chickenring:1>, <ore:ingotGold>], [<extrautils2:goldenlasso>.withTag({Animal: {id: "minecraft:bat"}}), <minecraft:elytra>, <extrautils2:goldenlasso:1>.withTag({Animal: {id: "minecraft:ghast"}})]]);
recipes.addShaped("angel_ring_1", <extrautils2:angelring:1>, [[<minecraft:feather:*>, <ore:ingotGold>, <minecraft:feather:*>], [<ore:ingotGold>, <extrautils2:chickenring:1>, <ore:ingotGold>], [<extrautils2:goldenlasso>.withTag({Animal: {id: "minecraft:bat"}}), <minecraft:elytra>, <extrautils2:goldenlasso:1>.withTag({Animal: {id: "minecraft:ghast"}})]]);
recipes.addShaped("angel_ring_2", <extrautils2:angelring:2>, [[<ore:dyePurple>, <ore:ingotGold>, <ore:dyePink>], [<ore:ingotGold>, <extrautils2:chickenring:1>, <ore:ingotGold>], [<extrautils2:goldenlasso>.withTag({Animal: {id: "minecraft:bat"}}), <minecraft:elytra>, <extrautils2:goldenlasso:1>.withTag({Animal: {id: "minecraft:ghast"}})]]);
recipes.addShaped("angel_ring_3", <extrautils2:angelring:3>, [[<minecraft:leather:*>, <ore:ingotGold>, <minecraft:leather:*>], [<ore:ingotGold>, <extrautils2:chickenring:1>, <ore:ingotGold>], [<extrautils2:goldenlasso>.withTag({Animal: {id: "minecraft:bat"}}), <minecraft:elytra>, <extrautils2:goldenlasso:1>.withTag({Animal: {id: "minecraft:ghast"}})]]);
recipes.addShaped("angel_ring_4", <extrautils2:angelring:4>, [[<ore:nuggetGold>, <ore:ingotGold>, <ore:nuggetGold>], [<ore:ingotGold>, <extrautils2:chickenring:1>, <ore:ingotGold>], [<extrautils2:goldenlasso>.withTag({Animal: {id: "minecraft:bat"}}), <minecraft:elytra>, <extrautils2:goldenlasso:1>.withTag({Animal: {id: "minecraft:ghast"}})]]);
recipes.addShaped("angel_ring_5", <extrautils2:angelring:5>, [[<minecraft:coal>, <ore:ingotGold>, <minecraft:coal:1>], [<ore:ingotGold>, <extrautils2:chickenring:1>, <ore:ingotGold>], [<extrautils2:goldenlasso>.withTag({Animal: {id: "minecraft:bat"}}), <minecraft:elytra>, <extrautils2:goldenlasso:1>.withTag({Animal: {id: "minecraft:ghast"}})]]);

recipes.removeByRecipeName("plethora:module_kinetic");
recipes.addShaped("module_kinetic", <plethora:module:4>, [[<minecraft:redstone_ore>, <minecraft:gold_ingot>, <minecraft:redstone_ore>], [<minecraft:sticky_piston>, <simplyjetpacks:itemjetpack:18>, <minecraft:sticky_piston>], [<minecraft:redstone_ore>, <minecraft:gold_ingot>, <minecraft:redstone_ore>]]);
