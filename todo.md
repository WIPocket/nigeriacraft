# Mods
- [x] Unidict
- [x] Some chunk protection?
- [x] More magic? (Botania, Blood Magic...)
- [x] Username misuse protection (Simple Login)
- [x] SimplyJetpacks?

# Configs
- [x] Make chickens end-game
- [x] Good default optifine config
- [x] Ore gen unification, balancing (not handled by unidict)
- [x] Make creative flight more endgame?
- [x] Nerf jetpacks (better progress = jetpacks -> elytra -> angel rings)
- [ ] Configs for different launcher
- [ ] Create a how-to documentation

# Improvements
- [x] Vending machines (Players' shops) --> Trading Station(Forestry)

